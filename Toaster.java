import java.util.Random;
public class Toaster{
	
	//Fields
	
	public String brand;
	public String quality;
	public double cost;
	
	//Methods
	
	public void grill(){
		Random random = new Random();
		int decision = random.nextInt(2);
		if(decision==1){
			System.out.println("You have succesfully burnt your bread. :)");
		}else{
			System.out.println("You have succesfully grilled your bread. :)");
		}
	}
	
	public void info(){
		System.out.println("The "+this.brand+" product offers a "+this.quality+" quality toaster that costs only "+this.cost+"$!");
	}
	
}