import java.util.Scanner;
public class ApplianceStore{
	
	public static void main(String[] args){
		Toaster[] toaster = new Toaster[4];
		Scanner keyboard = new Scanner(System.in); 
		for(int i = 0; i<toaster.length; i++){
			toaster[i] = new Toaster();
			System.out.println("What is the brand of the toaster?");
			toaster[i].brand = keyboard.next();
			System.out.println("The toaster has a good/bad quality?");
			toaster[i].quality = keyboard.next();
			System.out.println("What is the retail price of the toaster?");
			toaster[i].cost = keyboard.nextDouble();
		}
		System.out.println("The brand: "+toaster[toaster.length-1].brand);
		System.out.println("The quality: "+toaster[toaster.length-1].quality);
		System.out.println("The cost: "+toaster[toaster.length-1].cost);
		
		toaster[0].grill();
		toaster[0].info();
	}
	
}